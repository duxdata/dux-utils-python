from setuptools import setup

setup(
    name='dux_utils',
    version='2.5.0',
    description='DUX Data Helper Utilities module',
    url='https://bitbucket.org/duxdata/dux-utils-python',
    author='Pedro Dousseau @ DUX Data',
    author_email='pedro@duxdata.com.br',
    packages=['dux_utils'],
    install_requires=[
        'python-jose[cryptography]',
        'awscli',
        'boto3',
        'bottle',
        'bcrypt',
        'blessings',
        'cheroot',
        'distro',
        'markdown',
        'matplotlib',
        'mysqlclient',
        'orator',
        'pandas',
        'psutil',
        'pynamodb',
        'pyquery',
        'requests',
        'schema',
        'tinydb',
        'xmltodict',
        'pymodbus'
    ],
    zip_safe=False
)
