# -*- coding: utf-8 -*-

import secrets
import base64
import uuid
from jose import jwt
from dux_utils.core import dict_merge
from dux_utils.datetime import get_timestamp, utc
from datetime import timedelta


def unique_id(output='hex'):
    uuid_ = uuid.uuid4()
    if output == 'b64':
        uuid_ = base64.urlsafe_b64encode(uuid.uuid4().bytes).decode('utf-8').replace('=', '')
    elif output == 'b32':
        uuid_ = base64.b32encode(uuid.uuid4().bytes).decode('utf-8').replace('=', '')
    elif output == 'full':
        uuid_ = str(uuid_)
    else:
        uuid_ = uuid_.hex
    return uuid_


def secure_token(size=32, timestamp=True, strip=True, tmult=1000, output='b64'):
    bts = bytes(0)
    if timestamp:
        # Timestamp in seconds * tmult as 10 byte array. Eg.: If tmult = 1000 then timestamp is miliseconds.
        timestamp_bytes = int(get_timestamp() * tmult).to_bytes(8, byteorder='big')
        # Calculate random bytes size
        randomness = size - len(timestamp_bytes)
        # Strip null padding
        if strip:
            timestamp_bytes = timestamp_bytes.lstrip('\x00'.encode('utf-8'))
        # Merge timestamp and random
        bts = timestamp_bytes + secrets.token_bytes(randomness)
    else:
        bts = secrets.token_bytes(size)
    if output == 'hex':
        return bts.hex()
    elif output == 'b32':
        return base64.b32encode(bts).decode('utf-8').replace('=', '')
    return base64.urlsafe_b64encode(bts).decode('utf-8').replace('=', '')


def jwt_decode(key, secret, algorithms=None, options=None, audience=None, issuer=None, subject=None, access_token=None):
    """Key is the JWT encoded string. Secret is the secret string or a JWK dict list. Algorithms can be a list of accepted algorithms.
    """
    try:
        return jwt.decode(key, secret, algorithms, options, audience, issuer, subject, access_token)
    except:
        return False


def jwt_encode(claims, secret, algorithm='HS256', expire=1200):
    """Claims is a dictionary of JWT content. Secret is the secret string or a JWK dict list.
        Expire is the time in seconds to set the exp claim. If expire is set to False, the JWT won't have and expiration time.
        Most common algorithms used are HS256 (symmetric encrytion single secret) and RS256 (asymmetric public-private keys).
    """
    if expire:
        claims = dict_merge({'exp': int(get_timestamp(utc() + timedelta(seconds=expire)))}, claims)
    try:
        return jwt.encode(claims, secret, algorithm)
    except:
        return False
