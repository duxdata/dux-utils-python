# -*- coding: utf-8 -*-

from markdown import markdown


def html_page(html_content, stylesheet, title="", favicon="", robots=False):
    # Create HTML content
    html = '<!DOCTYPE html><html><head>'
    html = '{}<meta name="viewport" content="width=device-width, initial-scale=1">'.format(html)
    if title != "":
        html = '{}<title>{}</title>'.format(html, title)
    if favicon != "":
        html = '{}<link rel="shortcut icon" type="image/x-icon" href="{}"/>'.format(html, favicon)
        html = '{}<link rel="icon" type="image/png" href="{}"/>'.format(html, favicon)
    if stylesheet != "":
        html = '{}<link rel="stylesheet" href="{}"/>'.format(html, stylesheet)
    if robots is True:
        html = '{}<meta name="robots" content="noindex">'.format(html)
    return '{}</head><body>{}</body></html>'.format(html, html_content)


def file_to_html(filename):
    with open(filename, mode="r", encoding="utf-8") as mdfile:
        return markdown(mdfile.read(), extensions=['markdown.extensions.tables', 'markdown.extensions.fenced_code'], output_format='html5')


def str_to_html(mdown):
    return markdown(mdown, extensions=['markdown.extensions.tables'])
