# -*- coding: utf-8 -*-

from datetime import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta


def timedelta_str(minutes):
    if minutes < 1:
        return "< 1 min"
    elif minutes > 60 and minutes < 1440:
        hours = minutes // 60
        minutes = minutes % 60
        return "%d h %d min" % (hours, minutes)
    elif minutes >= 1440:
        days = minutes // 1440
        hours = (minutes % 1440) // 60
        minutes = (minutes % 1440) % 60
        return "%d d %d h %d min" % (days, hours, minutes)
    else:
        return "%d min" % (minutes)


def datetime_parse(date_str):
    return datetime(
        int(date_str[0:4]),
        int(date_str[5:7]),
        int(date_str[8:10]),
        int(date_str[11:13]),
        int(date_str[14:16]),
        int(date_str[17:19]),
    )


def datetime_str(dt):
    return dt.strftime("%Y-%m-%d %H:%M:%S")


def day_beginning(dt):
    return dt.replace(hour=0, minute=0, second=0)


def day_end(dt):
    return dt.replace(hour=23, minute=59, second=59)


def day_interval_list(date_start, date_end):
    date_gap = (date_end.replace(hour=0, minute=0, second=0) - date_start.replace(hour=0, minute=0, second=0)).days + 1
    return [(date_start + timedelta(days=x)) for x in range(0, date_gap)]


def week_beginning(dt):
    day_delta = dt.weekday() + 1
    if day_delta > 6:
        day_delta = 0
    return (dt - timedelta(days=day_delta)).replace(hour=0, minute=0, second=0)


def week_end(dt):
    return (dt + timedelta(days=(5 - dt.weekday()))).replace(hour=23, minute=59, second=59)


def month_beginning(dt):
    return dt.replace(day=1, hour=0, minute=0, second=0)


def month_end(dt):
    dt = dt.replace(day=28) + timedelta(days=4)
    return (dt - timedelta(days=dt.day)).replace(hour=23, minute=59, second=59)


def month_interval_list(date_start, date_end):
    date_gap = (date_end.year - date_start.year) * 12 + date_end.month - date_start.month + 1
    return [(date_start + relativedelta(months=+x)) for x in range(0, date_gap)]


def year_interval_list(date_start, date_end):
    date_gap = date_end.year - date_start.year + 1
    return [(date_start + relativedelta(years=+x)) for x in range(0, date_gap)]


def round_hour(dt):
    # Rounds to nearest hour by adding a timedelta hour if minute >= 30
    return (dt.replace(second=0, microsecond=0, minute=0, hour=dt.hour) + timedelta(hours=dt.minute // 30))


def get_timestamp(dt=None):
    if dt is None:
        dt = utc()
    return (dt - datetime.utcfromtimestamp(0)).total_seconds()


def from_timestamp(utc_timestamp):
    return datetime.utcfromtimestamp(utc_timestamp)


def utc():
    return datetime.utcnow()
