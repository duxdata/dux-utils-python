# -*- coding: utf-8 -*-

import sys
import subprocess
import psutil
import socket
import platform
import distro
from datetime import datetime


class ShellResult:

    status = False
    output = ""
    errors = ""

    def __init__(self, result):
        if result.returncode == 0:
            self.status = True
        if result.stdout is not None:
            self.output = result.stdout
        if result.stderr is not None:
            self.errors = result.stderr


def run(cmd, input=None, cwd=None, timeout=None, transp=False):
    """Execute shell command and get output
        cmd     -> <string> shell command to be executed
        input   -> <string> text input to the command
        cwd     -> <string> set working directory
        timeout -> <number> timeout in secs
        transp  -> <boolean> transparent execution (print output)
        Returns ShellResult class (status, output, errors)
    """

    result = ShellResult(subprocess.run(
        cmd,
        input=input,
        cwd=cwd,
        timeout=timeout,
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True,
        executable='/bin/bash'
    ))
    if transp:
        sys.stdout.write(result.output)
        sys.stdout.flush()
        if len(result.errors) > 0:
            print("\r\nSTDERR:\r\n")
            print(result.errors)
    return result


def spawn(cmd, input=None, cwd=None, detach=False):
    """Spawn new shell process
        cmd     -> <string> shell command to be executed
        input   -> <string> text input to the command
        cwd     -> <string> set working directory
        detach  -> <boolean> detach process from tree and set stds to /dev/null
        Returns proccess PID
    """
    pio = subprocess.DEVNULL if detach else subprocess.PIPE
    proc = subprocess.Popen(
        cmd,
        cwd=cwd,
        shell=True,
        stderr=subprocess.STDOUT,
        stdout=pio,
        stdin=pio,
        start_new_session=detach,
        executable='/bin/bash',
        universal_newlines=True
    )
    if input is not None and not detach:
        proc.stdin.write(input)
    if not detach:
        proc.stdin.close()
    return proc.pid


def pinfo(ppids=[], pnames=[], pports=[], pcmds=[], pattrs=[]):

    plist = []

    checkpid = len(ppids) > 0
    checkname = len(pnames) > 0
    checkports = len(pports) > 0
    checkcmd = len(pcmds) > 0

    if len(pattrs) == 0:
        pattrs = [
            'pid', 'name', 'cmdline', 'status', 'username', 'cpu_percent',
            'memory_percent', 'memory_info', 'connections', 'open_files', 'create_time'
        ]

    if checkname:
        pnames = [pname.lower() for pname in pnames]

    for proc in psutil.process_iter():
        pcheck = False
        try:
            if checkpid:
                pcheck = (proc.pid in ppids)
            if checkname and not pcheck:
                for pname in pnames:
                    pcheck = (pname in proc.name().lower())
            if checkports and not pcheck:
                for con in proc.connections():
                    if len(con.laddr):
                        pcheck = con.laddr[1] in pports
                    elif len(con.raddr):
                        pcheck = con.raddr[1] in pports
                    if pcheck:
                        break
            if checkcmd and not pcheck:
                for pcmd in pcmds:
                    pcheck = (pcmd in ','.join(proc.cmdline()))
        except:
            pass
        if pcheck:
            pinfo = proc.as_dict(pattrs)
            if 'open_files' in pinfo:
                if pinfo['open_files'] is not None:
                    pinfo['open_files'] = [f.path for f in pinfo['open_files']]
            if 'cmdline' in pinfo:
                if pinfo['cmdline'] is not None:
                    pinfo['cmdline'] = ' '.join(pinfo['cmdline'])
            if 'create_time' in pinfo:
                if pinfo['create_time'] is not None:
                    pinfo['create_time'] = datetime.fromtimestamp(pinfo['create_time']).strftime("%Y-%m-%d %H:%M:%S")
            if 'cpu_percent' in pinfo:
                if pinfo['cpu_percent'] is not None:
                    pinfo['cpu_percent'] = round(pinfo['cpu_percent'], 3)
            if 'memory_percent' in pinfo:
                if pinfo['memory_percent'] is not None:
                    pinfo['memory_percent'] = round(pinfo['memory_percent'], 3)
            if 'memory_info' in pinfo:
                if pinfo['memory_info'] is not None:
                    pinfo['memory_info'] = round(pinfo['memory_info'][0] / 1000000, 3)  # memory usage in MB
            if 'connections' in pinfo:
                if pinfo['connections'] is not None:
                    conns = []
                    for conn in pinfo['connections']:
                        conns.append({
                            'type': conn.status,
                            'local': ':'.join([str(x) for x in conn.laddr]),   # join ip and port
                            'remote': ':'.join([str(x) for x in conn.raddr])   # join ip and port
                        })
                    pinfo['connections'] = conns
            plist.append(pinfo)
    return plist


def sysinfo(system=True, cpu=True, memory=True, disk=True, network=False, conn_filter=['LISTEN', 'ESTABLISHED']):

    stats = {'time': datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')}

    # System
    if system:
        stats['system'] = {
            'hostname': platform.node(),
            'architecture': platform.machine(),
            'os': {
                'type': platform.system(),
                'details': platform.platform(),
                'release': platform.release(),
                'version': platform.version(),
                'distribution': distro.name(True),
            },
            'processor': platform.processor(),
        }

    # CPU
    if cpu:
        cpudetails_1 = psutil.cpu_percent(0.2)
        cpudetails_2 = psutil.cpu_percent(0.2, True)
        cpudetails_3 = psutil.cpu_times_percent(0.2)

        stats['cpu'] = {
            'usage': cpudetails_1,                                  # %
            'cores': cpudetails_2,                                  # %
            'user': cpudetails_3.user,                              # %
            'system': cpudetails_3.system,                          # %
            'idle': cpudetails_3.idle,                              # %
        }

    # Memory
    if memory:
        memdetails = psutil.virtual_memory()
        stats['memory'] = {
            'used': round(memdetails.used / 1000000000, 3),                 # GB
            'available': round(memdetails.available / 1000000000, 3),       # GB
            'total': round(memdetails.total / 1000000000, 3),               # GB
            'usage': round(memdetails.used / memdetails.total * 100, 1),    # %
        }

    # Disk
    if disk:
        diskdetails = psutil.disk_usage('/')
        stats['disk'] = {
            'used': round(diskdetails.used / 1000000000, 3),            # GB
            'available': round(diskdetails.free / 1000000000, 3),       # GB
            'total': round(diskdetails.total / 1000000000, 3),          # GB
            'usage': diskdetails.percent,                               # %
            'partitions': []
        }
        for part in psutil.disk_partitions():
            diskdetails = psutil.disk_usage(part.mountpoint)
            stats['disk']['partitions'].append({
                'device': part.device,
                'mountpoint': part.mountpoint,
                'type': part.fstype,
                'used': round(diskdetails.used / 1000000000, 3),        # GB
                'available': round(diskdetails.free / 1000000000, 3),   # GB
                'total': round(diskdetails.total / 1000000000, 3),      # GB
                'usage': diskdetails.percent                            # %
            })

    # Network
    if network:
        stats['network'] = {
            'interfaces': [],
            'connections': []
        }
        interfaces = psutil.net_if_addrs()
        for interface in interfaces:
            interfacedetails = {
                'name': interface
            }
            for address in interfaces[interface]:
                if address.family == socket.AF_INET:
                    interfacedetails['ip'] = {
                        'address': address.address,
                        'netmask': address.netmask,
                        'broadcast': address.broadcast
                    }
                elif address.family == socket.AF_INET6:
                    interfacedetails['ipv6'] = {
                        'address': address.address,
                        'netmask': address.netmask
                    }
                elif address.family == socket.AF_LINK:
                    interfacedetails['mac'] = address.address
            stats['network']['interfaces'].append(interfacedetails)
        for conn in psutil.net_connections():
            if conn.status in conn_filter:
                stats['network']['connections'].append({
                    'type': conn.status,
                    'local': ':'.join([str(x) for x in conn.laddr]),   # join ip and port
                    'remote': ':'.join([str(x) for x in conn.raddr]),   # join ip and port
                    'pid': conn.pid
                })

    return stats
