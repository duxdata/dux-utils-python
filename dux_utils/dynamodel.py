# -*- coding: utf-8 -*-

from dux_utils.schema import is_valid
from dux_utils.core import dict_merge
from pynamodb.models import Model
from pynamodb.attributes import (
    UnicodeAttribute,
    NumberAttribute,
    BooleanAttribute,
    UTCDateTimeAttribute,
    MapAttribute,
    ListAttribute,
    UnicodeSetAttribute,
    NumberSetAttribute,
    JSONAttribute
)


def dynattribute(attr_type):
    if attr_type == 'string':
        return UnicodeAttribute
    elif attr_type == 'number':
        return NumberAttribute
    elif attr_type == 'boolean':
        return BooleanAttribute
    elif attr_type == 'datetime':
        return UTCDateTimeAttribute
    elif attr_type == 'map':
        return MapAttribute
    elif attr_type == 'list':
        return ListAttribute
    elif attr_type == 'string_set':
        return UnicodeSetAttribute
    elif attr_type == 'number_set':
        return NumberSetAttribute
    elif attr_type == 'json':
        return JSONAttribute


class Dynamodel(Model):

    # Every model must have the lines below defining table name and region
    # class Meta:
    #     table_name = 'my-dynamo-table-name'
    #     region = 'us-west-2'

    # Attribute names related to Python objetct types. Eg.: 'name': str. Used in the validate method.
    schema = {}

    # Default values when exporting as dict. Used in the dict and validate methods.
    defaults = {}

    # Helper thtat defines how it is exported as dict.
    def __iter__(self):
        for name, attr in self.get_attributes().items():
            if isinstance(attr, MapAttribute):
                if getattr(self, name):
                    yield name, getattr(self, name).as_dict()
            elif isinstance(attr, ListAttribute) or isinstance(attr, UnicodeSetAttribute) or isinstance(attr, NumberSetAttribute):
                if getattr(self, name):
                    yield name, [el for el in getattr(self, name)]
            elif isinstance(attr, UTCDateTimeAttribute):
                if getattr(self, name):
                    yield name, attr.serialize(getattr(self, name))
            else:
                yield name, getattr(self, name)

    # Exports attributes as dict merged with defaults
    def dict(self):
        return dict_merge(self.defaults, dict(self))

    # Gets the dict and validates with schema
    def validate(self):
        return is_valid(self.schema, self.dict())
