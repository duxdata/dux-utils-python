# -*- coding: utf-8 -*-

import base64


def b64_add_padding(b64str):
    missing_padding = len(b64str) % 4
    if missing_padding != 0:
        b64str += '=' * (4 - missing_padding)
    return b64str


def b64decode(b64str, decode_type='strict'):
    return base64.b64decode(b64_add_padding(b64str).encode()).decode('utf-8', decode_type)


def b64decode_urlsafe(b64str, decode_type='strict'):
    return base64.urlsafe_b64decode(b64_add_padding(b64str).encode()).decode('utf-8', decode_type)


def b64encode(b64str, decode_type='strict'):
    return base64.b64encode(b64str.encode()).decode('utf-8', decode_type)


def b64encode_urlsafe(b64str, decode_type='strict'):
    return base64.urlsafe_b64encode(b64str.encode()).decode('utf-8', decode_type)
