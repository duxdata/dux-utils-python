# -*- coding: utf-8 -*-

from pymodbus.client.sync import ModbusTcpClient


def client(conf):
    try:
        return ModbusTcpClient(conf['ip'])
    except:
        return False


def write_coil(address, value, conf=None, session=None):
    try:
        close = False
        if session is None:
            session = client(conf)
            close = True
        resp = session.write_coil(address, value)
        if close is True:
            session.quit()
        return resp
    except:
        return False


def read_coil_bits(address, size, conf=None, session=None):
    try:
        close = False
        if session is None:
            session = client(conf)
            close = True
        result = session.read_coils(address, size)
        result = result.bits
        if close is True:
            session.quit()
        return result
    except:
        return False
