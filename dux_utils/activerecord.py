# -*- coding: utf-8 -*-

from orator import DatabaseManager
from orator import Schema as OratorSchema
from orator import Model
from orator import SoftDeletes


class Database(DatabaseManager):

    pass


class Schema(OratorSchema):

    def __init__(self, config):
        db = DatabaseManager(config)
        super().__init__(db)


class ActiveRecord(SoftDeletes, Model):

    __guarded__ = []
    __dates__ = ['deleted_at']

    @classmethod
    def setdb(cls, db):
        cls.set_connection_resolver(db)
