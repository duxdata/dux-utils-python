# -*- coding: utf-8 -*-

from io import StringIO
from datetime import datetime
import pandas as pd


def csv_file(
    path, delimiter=',', cols=None, index=0, title=None, dates=True, date_format='%Y-%m-%d %H:%M:%S', skip=0, nrows=None, header=None,
    decimal='.', thousands=None
):
    def dateparse(x):
        try:
            return datetime.strptime(x, date_format)
        except:
            return datetime.strptime('0', '%S')
    data = pd.read_csv(
        path, delimiter=delimiter, header=header, usecols=cols, index_col=index, names=title, skiprows=skip, nrows=None,
        parse_dates=dates, date_parser=dateparse, decimal=decimal, thousands=thousands
    )
    data.index.name = None
    if header is None and title is None:
        data.columns = range(len(data.columns))
    return data


def csv_string(
    cstr, delimiter=',', cols=None, index=0, title=None, dates=True, date_format='%Y-%m-%d %H:%M:%S', skip=0, nrows=None, header=None,
    decimal='.', thousands=None
):
    def dateparse(x):
        try:
            return datetime.strptime(x, date_format)
        except:
            return datetime.strptime('0', '%S')
    data = pd.read_csv(
        StringIO(cstr), delimiter=delimiter, header=header, usecols=cols, names=title, index_col=index, skiprows=skip, nrows=nrows,
        parse_dates=dates, date_parser=dateparse, decimal=decimal, thousands=thousands
    )
    data.index.name = None
    if dates:
        data.index = pd.to_datetime(data.index, format=date_format)
    if header is None and title is None:
        data.columns = range(len(data.columns))
    return data


def list(lst, cols=None, index=0, title=None, dates=True, date_format='%Y-%m-%d %H:%M:%S'):
    data = pd.DataFrame(lst)
    data.columns = range(len(data.columns))
    data = data.set_index(index)
    data.index.name = None
    if dates:
        data.index = pd.to_datetime(data.index, format=date_format)
    if cols is not None:
        data.columns = range(len(data.columns))
        data = data.filter(items=cols)
    if title is None:
        data.columns = range(len(data.columns))
    else:
        data.columns = title
    return data


def pandas():
    return pd


def merge(first, last, keep='last'):
    first = first.append(last)
    first.reset_index(inplace=True)
    first.drop_duplicates(subset='index', keep=keep, inplace=True)
    first.set_index('index', inplace=True)
    first.sort_index(kind='mergesort', inplace=True)
    first.index.name = None
    return first
