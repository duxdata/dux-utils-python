# -*- coding: utf-8 -*-

import boto3
from decimal import Decimal


class SNS:

    def __init__(self, region='us-west-2'):
        self.client = boto3.client('sns', region_name=region)

    def send_sms(self, text, recipients):
        """Send a sms through SNS
        """
        try:
            for recp in recipients:
                self.client.publish(
                    PhoneNumber=recp,
                    Message=text
                )
            return True
        except:
            return False


class S3:

    def __init__(self, bucket_name):
        self.bucket = boto3.resource('s3').Bucket(bucket_name)

    def get_object(self, key, dctype='text/plain'):
        """Gets an object from a bucket by its key
        """
        try:
            reponse = self.bucket.Object(key).get()
            return {
                "key": key,
                "content_type": reponse['ContentType'],
                "content": reponse['Body'].read().decode('utf-8')
            }
        except:
            return {
                "key": key,
                "content_type": dctype,
                "content": ""
            }

    def put_object(self, s3obj):
        """Creates a s3 object. If exists, then it is updated.
            s3obj = {
                "key": key,
                "content": "object content"
                "content_type": "text/csv; application/json; ..."
            }
        """
        try:
            self.bucket.put_object(
                Key=s3obj["key"],
                Body=s3obj["content"],
                ContentType=s3obj["content_type"]
            )
            return True
        except:
            return False


class SES:

    def __init__(self, region='us-west-2'):
        self.client = boto3.client('ses', region_name=region)

    def send_email(self, source, destinations, subject, body, reply_tos=[]):
        """Send an e-mail through SES
        """
        try:
            if len(reply_tos) == 0:
                reply_tos.append(source)
            self.client.send_email(
                Destination={
                    'ToAddresses': destinations
                },
                Message={
                    'Body': {
                        'Html': {
                            'Charset': 'UTF-8',
                            'Data': body
                        }
                    },
                    'Subject': {
                        'Charset': 'UTF-8',
                        'Data': subject
                    }
                },
                ReplyToAddresses=reply_tos,
                Source=source
            )
            return True
        except:
            return False

    def send_email_with_attachment(self, source, destinations, subject, body, att_content, att_name, reply_tos=[]):
        """Send e-mail with file attachment
        """
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText
        from email.mime.application import MIMEApplication
        try:
            if len(reply_tos) == 0:
                reply_tos.append(source)
            ebody = MIMEMultipart('alternative')
            ebody.attach(MIMEText(body.encode("UTF-8"), 'plain', "UTF-8"))
            eatt = MIMEApplication(att_content)
            eatt.add_header('Content-Disposition', 'attachment', filename=att_name)
            msg = MIMEMultipart('mixed')
            msg['Subject'] = subject
            msg['From'] = source
            msg['To'] = ','.join(destinations)
            msg['reply-to'] = ','.join(reply_tos)
            msg.attach(ebody)
            msg.attach(eatt)
            self.client.send_raw_email(
                Destinations=destinations,
                Source=source,
                RawMessage={
                    'Data': msg.as_string(),
                }
            )
            return True
        except:
            return False


class Dynamo:

    def __init__(self, table_name):
        self.table = boto3.resource('dynamodb', region_name='us-west-2').Table(table_name)

    def add_item(self, pkey_name, item):
        """Add an item to table if primary key does not exist
        """
        try:
            self.table.put_item(
                Item=item,
                ConditionExpression=boto3.dynamodb.conditions.Attr(pkey_name).not_exists()
            )
            return True
        except:
            return False

    def delete_item(self, pkey_name, pkey_val):
        """Add an item to table if primary key does not exist
        """
        try:
            self.table.delete_item(
                Key={
                    pkey_name: pkey_val
                },
                ConditionExpression=boto3.dynamodb.conditions.Attr(pkey_name).exists()
            )
            return True
        except:
            return False

    def get_item(self, pkey_name, pkey_val, attributes=[], consistent=False):
        """Add an item to table if primary key does not exist
        """
        try:
            response = {}
            if len(attributes) > 0:
                response = self.table.get_item(
                    Key={
                        pkey_name: pkey_val
                    },
                    AttributesToGet=attributes,
                    ConsistentRead=consistent
                )['Item']
            else:
                response = self.table.get_item(
                    Key={
                        pkey_name: pkey_val
                    },
                    ConsistentRead=consistent
                )['Item']
            for key in response:
                if isinstance(response[key], Decimal) and response[key] is not None:
                    response[key] = float(response[key])
            return response
        except:
            return None

    def update_item(self, pkey_name, pkey_val, updates):
        """Update item if primary key exists on table
            updates = {
                attr_key: {
                    Value: attr_val
                    Action: ADD | PUT | DELETE
                }
            }
        """
        try:
            self.table.update_item(
                Key={
                    pkey_name: pkey_val
                },
                AttributeUpdates=updates
            )
            return True
        except:
            return False
