# -*- coding: utf-8 -*-

import sys
import os
import json


def to_json(obj, pretty=False, indent=4):
    if pretty:
        return json.dumps(obj, indent=indent, ensure_ascii=False).encode('utf8').decode('utf8')
    else:
        return json.dumps(obj, separators=(',', ':'), ensure_ascii=False).encode('utf8').decode('utf8')


def sort_list(l, i=None):
    """Sort a list. If a index is passed, it will sort by the index.
    """
    if i is None:
        return sorted(l)
    return sorted(l, key=lambda k: k[i])


def dict_merge(x, y):
    """Merge dictionaries. The second will override matching keys of the first.
    """
    z = x.copy()
    z.update(y)
    return z


def dict_contains(obj, key):
    """Check if dictionary has key recursively.
    """
    if key in obj:
        return True
    for k, v in obj.items():
        if isinstance(v, dict):
            return dict_contains(v, key)
    return False


def path_add(path):
    """Insert directory to path.
    """
    sys.path.insert(1, path)


def path_invert():
    """Move current directory to end of path so that installed packages have priority on imports.
    """
    sys.path.append(sys.path.pop(0))


def path_basename(path):
    """Get basename from path
    """
    return os.path.basename(path)


def dir_path(dir):
    """Get full path of directory or file.
    """
    return os.path.dirname(os.path.realpath(dir))


def dir_script():
    """Get full directory path of current script.
    """
    return dir_path(sys.argv[0])


def dir_up(dir=None):
    """Get parent fold of directory
    """
    if dir is None:
        return os.path.dirname(dir_script())
    return os.path.dirname(dir_path(dir))
