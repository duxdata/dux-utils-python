# -*- coding: utf-8 -*-

import gzip
import io


def gzip_str(unzipped_str):
    out = io.BytesIO()
    with gzip.GzipFile(fileobj=out, mode='w') as fo:
        fo.write(unzipped_str.encode())
    return out.getvalue()


def gunzip_bytes(zipped_bytes):
    in_ = io.BytesIO()
    in_.write(zipped_bytes)
    in_.seek(0)
    with gzip.GzipFile(fileobj=in_, mode='rb') as fo:
        gunzipped_bytes_obj = fo.read()
    return gunzipped_bytes_obj.decode('utf-8', 'ignore')
