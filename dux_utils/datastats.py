# -*- coding: utf-8 -*-

# import statistics
# import dux_analytics_services.utils as dux_utils
# from datetime import timedelta
#
#
# pinfinity = float('inf')
# minfinity = float('-inf')
#
#
# def daily_average(data):
#     """Calculate daily average for a given dataset.
#         Data format must be: [[Datetime, Value],...,[Datetime, Value]] ordered by date.
#     """
#     ref_date = dux_utils.day_beginning(data[0][0])
#     stop_date = dux_utils.day_end(data[-1][0])
#     data_index = 0
#     stat_arr = []
#     arr_size = len(data)
#
#     while ref_date < stop_date:
#         next_date = ref_date + timedelta(hours=24)
#         daily_count = 0
#         daily_sum = 0.0
#         while data[data_index][0] < next_date:
#             if data[data_index][1] is not None:
#                 daily_sum = daily_sum + float(data[data_index][1])
#                 daily_count = daily_count + 1
#             data_index = data_index + 1
#             if data_index >= arr_size:
#                 break
#         if daily_count > 0:
#             mean = round(daily_sum / daily_count, 4)
#             if mean == mean and mean != minfinity and mean != pinfinity:
#                 stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), mean])
#             else:
#                 stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), None])
#         else:
#             stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), None])
#         ref_date = next_date
#
#     return stat_arr
#
#
# def daily_max(data):
#     """Calculate daily maximum for a given dataset.
#         Data format must be: [[Datetime, Value],...,[Datetime, Value]] ordered by date.
#     """
#     ref_date = dux_utils.day_beginning(data[0][0])
#     stop_date = dux_utils.day_end(data[-1][0])
#     data_index = 0
#     stat_arr = []
#     arr_size = len(data)
#
#     while ref_date < stop_date:
#         next_date = ref_date + timedelta(hours=24)
#         max = minfinity
#         while data[data_index][0] < next_date:
#             if data[data_index][1] is not None:
#                 curr_val = float(data[data_index][1])
#                 if curr_val > max:
#                     max = curr_val
#             data_index = data_index + 1
#             if data_index >= arr_size:
#                 break
#         if max != minfinity:
#             stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), round(max, 4)])
#         else:
#             stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), None])
#         ref_date = next_date
#
#     return stat_arr
#
#
# def daily_min(data):
#     """Calculate daily minimum for a given dataset.
#         Data format must be: [[Datetime, Value],...,[Datetime, Value]] ordered by date.
#     """
#     ref_date = dux_utils.day_beginning(data[0][0])
#     stop_date = dux_utils.day_end(data[-1][0])
#     data_index = 0
#     stat_arr = []
#     arr_size = len(data)
#
#     while ref_date < stop_date:
#         next_date = ref_date + timedelta(hours=24)
#         min = pinfinity
#         while data[data_index][0] < next_date:
#             if data[data_index][1] is not None:
#                 curr_val = float(data[data_index][1])
#                 if curr_val < min:
#                     min = curr_val
#             data_index = data_index + 1
#             if data_index >= arr_size:
#                 break
#         if min != pinfinity:
#             stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), round(min, 4)])
#         else:
#             stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), None])
#         ref_date = next_date
#
#     return stat_arr
#
#
# def daily_sum(data):
#     """Calculate daily sum for a given dataset.
#         Data format must be: [[Datetime, Value],...,[Datetime, Value]] ordered by date.
#     """
#     ref_date = dux_utils.day_beginning(data[0][0])
#     stop_date = dux_utils.day_end(data[-1][0])
#     data_index = 0
#     stat_arr = []
#     arr_size = len(data)
#
#     while ref_date < stop_date:
#         next_date = ref_date + timedelta(hours=24)
#         sum = 0.0
#         while data[data_index][0] < next_date:
#             if data[data_index][1] is not None:
#                 sum = sum + float(data[data_index][1])
#             data_index = data_index + 1
#             if data_index >= arr_size:
#                 break
#         stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), round(sum, 4)])
#         ref_date = next_date
#
#     return stat_arr
#
#
# def weekly_average(data):
#     """Calculate weekly average for a given dataset.
#         Data format must be: [[Datetime, Value],...,[Datetime, Value]] ordered by date.
#     """
#     ref_date = dux_utils.week_beginning(data[0][0])
#     stop_date = dux_utils.week_end(data[-1][0])
#     data_index = 0
#     stat_arr = []
#     arr_size = len(data)
#
#     while ref_date < stop_date:
#         next_date = ref_date + timedelta(days=7)
#         weekly_count = 0
#         weekly_sum = 0.0
#         while data[data_index][0] < next_date:
#             if data[data_index][1] is not None:
#                 weekly_sum = weekly_sum + float(data[data_index][1])
#                 weekly_count = weekly_count + 1
#             data_index = data_index + 1
#             if data_index >= arr_size:
#                 break
#         if weekly_count > 0:
#             mean = round(weekly_sum / weekly_count, 4)
#             if mean == mean and mean != minfinity and mean != pinfinity:
#                 stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), mean])
#             else:
#                 stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), None])
#         else:
#             stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), None])
#         ref_date = next_date
#
#     return stat_arr
#
#
# def weekly_max(data):
#     """Calculate weekly maximum for a given dataset.
#         Data format must be: [[Datetime, Value],...,[Datetime, Value]] ordered by date.
#     """
#     ref_date = dux_utils.week_beginning(data[0][0])
#     stop_date = dux_utils.week_end(data[-1][0])
#     data_index = 0
#     stat_arr = []
#     arr_size = len(data)
#
#     while ref_date < stop_date:
#         next_date = ref_date + timedelta(days=7)
#         max = minfinity
#         while data[data_index][0] < next_date:
#             if data[data_index][1] is not None:
#                 curr_val = float(data[data_index][1])
#                 if curr_val > max:
#                     max = curr_val
#             data_index = data_index + 1
#             if data_index >= arr_size:
#                 break
#         if max != minfinity:
#             stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), round(max, 4)])
#         else:
#             stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), None])
#         ref_date = next_date
#
#     return stat_arr
#
#
# def weekly_min(data):
#     """Calculate weekly minimum for a given dataset.
#         Data format must be: [[Datetime, Value],...,[Datetime, Value]] ordered by date.
#     """
#     ref_date = dux_utils.week_beginning(data[0][0])
#     stop_date = dux_utils.week_end(data[-1][0])
#     data_index = 0
#     stat_arr = []
#     arr_size = len(data)
#
#     while ref_date < stop_date:
#         next_date = ref_date + timedelta(days=7)
#         min = pinfinity
#         while data[data_index][0] < next_date:
#             if data[data_index][1] is not None:
#                 curr_val = float(data[data_index][1])
#                 if curr_val < min:
#                     min = curr_val
#             data_index = data_index + 1
#             if data_index >= arr_size:
#                 break
#         if min != pinfinity:
#             stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), round(min, 4)])
#         else:
#             stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), None])
#         ref_date = next_date
#
#     return stat_arr
#
#
# def weekly_sum(data):
#     """Calculate weekly sum for a given dataset.
#         Data format must be: [[Datetime, Value],...,[Datetime, Value]] ordered by date.
#     """
#     ref_date = dux_utils.week_beginning(data[0][0])
#     stop_date = dux_utils.week_end(data[-1][0])
#     data_index = 0
#     stat_arr = []
#     arr_size = len(data)
#
#     while ref_date < stop_date:
#         next_date = ref_date + timedelta(days=7)
#         sum = 0.0
#         while data[data_index][0] < next_date:
#             if data[data_index][1] is not None:
#                 sum = sum + float(data[data_index][1])
#             data_index = data_index + 1
#             if data_index >= arr_size:
#                 break
#         stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), round(sum, 4)])
#         ref_date = next_date
#
#     return stat_arr
#
#
# def monthly_average(data):
#     """Calculate monthly average for a given dataset.
#         Data format must be: [[Datetime, Value],...,[Datetime, Value]] ordered by date.
#     """
#     ref_date = dux_utils.month_beginning(data[0][0])
#     stop_date = dux_utils.month_end(data[-1][0])
#     data_index = 0
#     stat_arr = []
#     arr_size = len(data)
#
#     while ref_date < stop_date:
#         next_date = dux_utils.day_beginning(dux_utils.month_end(ref_date) + timedelta(days=1))
#         monthly_count = 0
#         monthly_sum = 0.0
#         while data[data_index][0] < next_date:
#             if data[data_index][1] is not None:
#                 monthly_sum = monthly_sum + float(data[data_index][1])
#                 monthly_count = monthly_count + 1
#             data_index = data_index + 1
#             if data_index >= arr_size:
#                 break
#         if monthly_count > 0:
#             mean = round(monthly_sum / monthly_count, 4)
#             if mean == mean and mean != minfinity and mean != pinfinity:
#                 stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), mean])
#             else:
#                 stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), None])
#         else:
#             stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), None])
#         ref_date = next_date
#
#     return stat_arr
#
#
# def monthly_max(data):
#     """Calculate monthly maximum for a given dataset.
#         Data format must be: [[Datetime, Value],...,[Datetime, Value]] ordered by date.
#     """
#     ref_date = dux_utils.month_beginning(data[0][0])
#     stop_date = dux_utils.month_end(data[-1][0])
#     data_index = 0
#     stat_arr = []
#     arr_size = len(data)
#
#     while ref_date < stop_date:
#         next_date = dux_utils.day_beginning(dux_utils.month_end(ref_date) + timedelta(days=1))
#         max = minfinity
#         while data[data_index][0] < next_date:
#             if data[data_index][1] is not None:
#                 curr_val = float(data[data_index][1])
#                 if curr_val > max:
#                     max = curr_val
#             data_index = data_index + 1
#             if data_index >= arr_size:
#                 break
#         if max != minfinity:
#             stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), round(max, 4)])
#         else:
#             stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), None])
#         ref_date = next_date
#
#     return stat_arr
#
#
# def monthly_min(data):
#     """Calculate monthly minimum for a given dataset.
#         Data format must be: [[Datetime, Value],...,[Datetime, Value]] ordered by date.
#     """
#     ref_date = dux_utils.month_beginning(data[0][0])
#     stop_date = dux_utils.month_end(data[-1][0])
#     data_index = 0
#     stat_arr = []
#     arr_size = len(data)
#
#     while ref_date < stop_date:
#         next_date = dux_utils.day_beginning(dux_utils.month_end(ref_date) + timedelta(days=1))
#         min = pinfinity
#         while data[data_index][0] < next_date:
#             if data[data_index][1] is not None:
#                 curr_val = float(data[data_index][1])
#                 if curr_val < min:
#                     min = curr_val
#             data_index = data_index + 1
#             if data_index >= arr_size:
#                 break
#         if min != pinfinity:
#             stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), round(min, 4)])
#         else:
#             stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), None])
#         ref_date = next_date
#
#     return stat_arr
#
#
# def monthly_sum(data):
#     """Calculate monthly sum for a given dataset.
#         Data format must be: [[Datetime, Value],...,[Datetime, Value]] ordered by date.
#     """
#     ref_date = dux_utils.month_beginning(data[0][0])
#     stop_date = dux_utils.month_end(data[-1][0])
#     data_index = 0
#     stat_arr = []
#     arr_size = len(data)
#
#     while ref_date < stop_date:
#         next_date = dux_utils.day_beginning(dux_utils.month_end(ref_date) + timedelta(days=1))
#         sum = 0.0
#         while data[data_index][0] < next_date:
#             if data[data_index][1] is not None:
#                 sum = sum + float(data[data_index][1])
#             data_index = data_index + 1
#             if data_index >= arr_size:
#                 break
#         stat_arr.append([ref_date.strftime('%Y-%m-%d %H:%M:%S'), round(sum, 4)])
#         ref_date = next_date
#
#     return stat_arr
#
#
# def arr_stats(data):
#     """Calculate minimum, mean, maximum and standard deviation for a given dataset.
#         Data format must be: [[Datetime, Value],...,[Datetime, Value]]
#         Returns [min, mean, max, stdev]
#     """
#     arr = [float(x[1]) for x in data if x[1] is not None]
#     arr_stats = [
#         min(arr),
#         statistics.mean(arr),
#         max(arr),
#         0
#     ]
#     if len(arr) > 1:
#         arr_stats[3] = statistics.stdev(arr)
#     return arr_stats
