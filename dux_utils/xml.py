# -*- coding: utf-8 -*-

import xmltodict
from pyquery import PyQuery


def parse(content):
    return xmltodict.parse(content)


def query(content):
    return PyQuery(content)
