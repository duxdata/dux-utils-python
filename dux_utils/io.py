# -*- coding: utf-8 -*-
import asyncio
import ssl
import bottle
import socket as sock
from cheroot import wsgi
from cheroot.ssl.builtin import BuiltinSSLAdapter


class HTTPSSL(bottle.ServerAdapter):
    # Create our own sub-class of Bottle's ServerAdapter
    # so that we can specify SSL. Using just server='cherrypy'
    # uses the default cherrypy server, which doesn't use SSL
    # By default, the server will allow negotiations with extremely old protocols
    # that are susceptible to attacks, so we only allow TLSv1.2
    def run(self, handler):
        server = wsgi.Server((self.host, self.port), handler)
        server.ssl_adapter = BuiltinSSLAdapter('cert.pem', 'privkey.pem')
        server.ssl_adapter.context.options |= ssl.OP_NO_TLSv1
        server.ssl_adapter.context.options |= ssl.OP_NO_TLSv1_1
        try:
            server.start()
        finally:
            server.stop()


def socket(address, callback, mode='client', type='tcp', timeout=None):
    if mode == 'server':
        # callback must be an async function that receives asyncio.StreamReader and asyncio.StreamWriter objects
        # see: https://docs.python.org/3/library/asyncio-stream.html#asyncio.StreamReader
        loop = asyncio.get_event_loop()
        server_def = None
        if type == 'tcp':
            address = address.split(':')
            server_def = asyncio.start_server(callback, address[0], address[1], loop=loop)
        if type == 'unix':
            server_def = asyncio.start_unix_server(callback, path=address, loop=loop)
        server = loop.run_until_complete(server_def)
        try:
            loop.run_forever()
        except KeyboardInterrupt:
            pass
        server.close()
        loop.run_until_complete(server.wait_closed())
        loop.close()
    elif mode == 'client':
        # callback must be a function that receives a socket object
        # see: https://docs.python.org/3/library/socket.html#socket-objects
        sock_ = None
        if type == 'tcp':
            address = address.split(':')
            address = (address[0], address[1])
            sock_ = sock.socket(sock.AF_INET, sock.SOCK_STREAM)
        elif type == 'unix':
            sock_ = sock.socket(sock.AF_UNIX, sock.SOCK_STREAM)
        if timeout is not None:
            sock_.settimeout(timeout)
        sock_.connect(address)
        callback(sock_)


def http(host, port, routes, quiet=False, debug=None, reloader=False, interval=1, plugins=None, config=None, ssl=False):
    for route in routes:
        bottle.route(route['path'], route['method'], route['callback'])
    if ssl:
        bottle.run(
            host=host, port=port, interval=interval, reloader=reloader, quiet=quiet,
            plugins=plugins, debug=debug, config=config, server=HTTPSSL
        )
    else:
        bottle.run(host=host, port=port, interval=interval, reloader=reloader, quiet=quiet, plugins=plugins, debug=debug, config=config)
