# -*- coding: utf-8 -*-

import sys
import codecs
import cgitb
import json
import urllib.parse
from os import environ
from ipaddress import ip_address
from socket import gethostbyname
from dux_utils.core import dict_merge


def init(content_type='application/json', headers=[], body=None):
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout.detach())
    for h in headers:
        print(h)
    if body is None:
        print('Content-Type: {};charset=utf-8\r\n'.format(content_type))
    else:
        print('Content-Length: {}'.format(len(body.encode('utf-8'))))
        print('Content-Type: {};charset=utf-8\r\n'.format(content_type))
        print(body)


def enable_debug():
    cgitb.enable(format='text')


def crud_action():
    return environ['REQUEST_METHOD']


def crud_execute(crud_create=None, crud_read=None, crud_update=None, crud_delete=None):
    crud_switcher = {
        'GET': crud_read,
        'POST': crud_create,
        'PUT': crud_update,
        'PATCH': crud_update,
        'DELETE': crud_delete
    }
    crud = crud_switcher[crud_action()]
    if crud is not None:
        return crud()
    return None


def crud_params():
    crud_params = get_params()
    if crud_action() != 'GET':
        crud_params = dict_merge(crud_params, get_body(data_json=True))
    return crud_params


def get_method():
    return environ['REQUEST_METHOD']


def get_uri():
    return environ['REQUEST_URI']


def get_ip():
    if 'HTTP_X_REAL_IP' in environ:
        return environ['HTTP_X_REAL_IP']
    elif 'REMOTE_ADDR' in environ:
        return environ['REMOTE_ADDR']


def get_headers():
    headers = {}
    for headername, headervalue in environ.items():
        if headername.startswith('HTTP_'):
            headers[headername.replace('HTTP_', '')] = headervalue
    return headers


def get_vars():
    vars = {}
    for headername, headervalue in environ.items():
        if not headername.startswith('HTTP_'):
            vars[headername] = headervalue
    return vars


def get_params():
    params = {}
    query = get_uri().split('?', 1)[-1].strip()
    query = urllib.parse.parse_qs(query)
    for prm in query:
        if len(query[prm]) > 1:
            params[prm] = query[prm]
        else:
            params[prm] = query[prm][0]
    return params


def get_body(data_json=False, data_binary=False):
    body = []
    try:
        body = sys.stdin.buffer.read()
        if data_binary is False:
            body = body.decode('utf-8', errors="ignore")
            if data_json:
                body = json.loads(body)
    except:
        pass
    return body


def bearer_token():
    token = None
    headers = get_headers()
    if 'AUTHORIZATION' in headers:
        if 'Bearer' in headers['AUTHORIZATION'] or 'bearer' in headers['AUTHORIZATION']:
            token = headers['AUTHORIZATION'].split()[1].strip()
    return token


def set_cookie(name, value, domain=None, path='/', maxage=3600, secure=True, httponly=True, samesite=None):
    cstr = 'Set-Cookie: {}={}; Max-Age={}'.format(name, value, maxage)
    if domain is not None:
        cstr = cstr + '; Domain={}; Path={}'.format(domain, path)
    if secure:
        cstr = cstr + '; Secure'
    if httponly:
        cstr = cstr + '; HttpOnly'
    if samesite is not None:
        cstr = cstr + '; SameSite={}'.format(samesite)
    return cstr


def get_cookie(name):
    headers = get_headers()
    if 'COOKIE' in headers:
        for cookie in headers['COOKIE'].split(';'):
            cookie = cookie.strip().split('=')
            if name == cookie[0].strip():
                return cookie[1].strip()
    return None


def nslookup(domain):
    return gethostbyname(domain)


def isprivate(vpn=None):
    req_ip = get_ip()
    inside_vpn = False
    if vpn is not None:
        inside_vpn == nslookup(vpn)
    return ip_address(req_ip).is_private or inside_vpn
