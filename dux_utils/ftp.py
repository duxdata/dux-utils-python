# -*- coding: utf-8 -*-

import ftplib
from io import BytesIO
from dux_utils.core import path_basename
from datetime import datetime


def client(conf):
    try:
        session = None
        if ':' in conf['host']:
            conf['host'] = conf['host'].split(':')
            session = ftplib.FTP()
            session.connect(conf['host'][0], int(conf['host'][1]))
            session.login(conf['user'], conf['pass'])
        else:
            session = ftplib.FTP(host=conf['host'], user=conf['user'], passwd=conf['pass'])
        if 'path' in conf:
            session.cwd(conf['path'])
        return session
    except:
        return False


def read(filename, conf=None, session=None):
    try:
        close = False
        if session is None:
            session = client(conf)
            close = True
        f = []

        def writebytes(b):
            f.append(b)

        session.retrbinary('RETR ' + filename, writebytes)
        if close is True:
            session.quit()
        return bytes().join(f)
    except:
        return False


def write(content, remote_filename, conf=None, session=None):
    try:
        close = False
        if session is None:
            session = client(conf)
            close = True
        f = BytesIO(content)
        session.storbinary('STOR ' + remote_filename, f)
        if close is True:
            session.quit()
        return True
    except:
        return False


def download(filename, conf=None, session=None):
    try:
        close = False
        if session is None:
            session = client(conf)
            close = True
        with open(path_basename(filename), 'wb') as f:
            session.retrbinary('RETR ' + filename, f.write)
        if close is True:
            session.quit()
        return True
    except:
        return False


def upload(local_filename, remote_filename, conf=None, session=None):
    try:
        close = False
        if session is None:
            session = client(conf)
            close = True
        with open(local_filename, 'rb') as f:
            session.storbinary('STOR ' + remote_filename, f)
        if close is True:
            session.quit()
        return True
    except:
        return False


def move(filename, new_filename, conf=None, session=None):
    try:
        close = False
        if session is None:
            session = client(conf)
            close = True
        session.rename(filename, new_filename)
        if close is True:
            session.quit()
        return True
    except:
        return False


def delete(filename, conf=None, session=None):
    try:
        close = False
        if session is None:
            session = client(conf)
            close = True
        session.delete(filename)
        if close:
            session.quit()
        return True
    except:
        return False


def modified(filename, conf=None, session=None):
    try:
        close = False
        if session is None:
            session = client(conf)
            close = True
        timestamp = session.voidcmd('MDTM {}'.format(filename))[4:].strip()
        timestamp = datetime.strptime(timestamp, '%Y%m%d%H%M%S')
        if close:
            session.quit()
        return timestamp
    except:
        return False


def mkdir(dname, conf=None, session=None):
    try:
        close = False
        if session is None:
            session = client(conf)
            close = True
        session.mkd(dname)
        if close is True:
            session.quit()
        return True
    except:
        return False


def list(path=None, conf=None, session=None):
    try:
        files = []
        close = False
        if session is None:
            session = client(conf)
            close = True
        if path is None:
            files = session.nlst()
        else:
            files = session.nlst(path)
        if close:
            session.quit()
        return files
    except:
        return []
