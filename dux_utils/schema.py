# -*- coding: utf-8 -*-

from schema import Schema


def is_valid(source, target):
    return Schema(source).is_valid(target)
