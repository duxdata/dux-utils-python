# -*- coding: utf-8 -*-

import bcrypt
import string
import base64
import hashlib
import random
from datetime import datetime


def random_key(n=8, output='full'):
    key = ''.join(random.choices(string.ascii_letters + string.digits, k=n)).encode()
    if output == 'b64':
        key = base64.urlsafe_b64encode(key).decode('utf-8').replace('=', '')
    elif output == 'b32':
        key = base64.b32encode(key).decode('utf-8').replace('=', '')
    elif output == 'full':
        key = key.decode('utf-8')
    else:
        key = hashlib.md5(key).hexdigest()

    return key


def unique_key(n=8):
    return hashlib.md5(
        "{}{}".format(
            int(datetime.utcnow().timestamp()),
            ''.join(random.choices(string.ascii_uppercase, k=n))
        ).encode()
    ).hexdigest()


def password(password):
    return bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')


def verify_password(password, hashed):
    return bcrypt.checkpw(password.encode('utf-8'), hashed.encode('utf-8'))
