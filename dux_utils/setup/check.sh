#!/bin/bash
################################################################################
# Title:        DUX Data Utils Check Seupt
# Description:  Check README for more information.
# Author:       Pedro Dousseau @ DUX Data
# Copyright:    DUX Data
# Date:         October 2018
# Version:      2.0.0
################################################################################

PYTHON_VERSION="null"

if [ -n "$1" ]; then
    if [ -n "$(which python$1)" ]; then
        PYTHON_VERSION="$1"
    fi
fi

if [[ $PYTHON_VERSION == "null" ]]; then

echo "Plase, provide a installed python version as argument."

else

echo "PYTHON VERSION: $PYTHON_VERSION"

# NEXT LINES MUST NOT BE IDENTED OTHERWISE IT WILL RAISE UNEXPECTED END OF LINE
# Check PIP requirements
eval "python$PYTHON_VERSION"<<-END
import os
os.chdir('..')
req_file = "{}/requirements.txt".format(os.getcwd())
os.chdir('/')
with open(req_file, 'r', encoding='utf-8') as fhd:
    for line in fhd:
        line = line.split("=")[0]
        line = line.split(">")[0]
        line = line.split("<")[0]
        line = line.strip()
        line = line.replace("mysqlclient", "MySQLdb").replace("python-jose[cryptography]", "jose")
        line = line.replace("requests-futures", "requests_futures")
        try:
            exec("import " + line)
            print("[OK] Found module: {} ".format(line))
        except:
            print("[ERROR] Missing module: {} ".format(line))
END

# Check if Python can import module
eval "python$PYTHON_VERSION"<<-END
import os
os.chdir('/')
try:
    import dux_utils
    from dux_utils import *
    print("[OK] DUX Utils available in python. Version: {}".format(dux_utils.__version__))
except:
    print("[ERROR] DUX Utils not available in python")
END

fi
