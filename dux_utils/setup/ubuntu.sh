#!/bin/bash
################################################################################
# Title:        DUX Data Utils Python Library Ubuntu Setup
# Description:  Check README for more information.
# Author:       Pedro Dousseau @ DUX Data
# Copyright:    DUX Data
# Date:         October 2018
# Version:      2.0.0
################################################################################

PACKAGE_NAME="dux_utils"
PYTHON_VERSION="null"

if [ -n "$1" ]; then
    if [ -n "$(which python$1)" ]; then
        PYTHON_VERSION="$1"
    fi
fi

if [[ $PYTHON_VERSION == "null" ]]; then
    echo "Plase, provide a installed python version as argument."
else
    echo "PYTHON VERSION: $PYTHON_VERSION"

    echo -e "\r\nUPDATING GIT REPOSITORY..."
    git pull

    echo -e "\r\nINSTALLING REQUIREMENTS..."
    sudo apt-get update
    sudo apt-get install build-essential python3-dev libmysqlclient-dev
    eval "sudo -H python$PYTHON_VERSION -m pip install -r ../requirements.txt --force-reinstall"
    PIP_DIRECTORY="/usr/local/lib/python$PYTHON_VERSION/dist-packages/"
    if test -d "/usr/local/lib/python$PYTHON_VERSION/site-packages/__pycache__"; then
        PIP_DIRECTORY="/usr/local/lib/python$PYTHON_VERSION/site-packages/"
    fi
    cd ..
    sudo mkdir -p $PIP_DIRECTORY
    sudo rm -f $PIP_DIRECTORY$PACKAGE_NAME
    sudo ln -s "$(pwd)" $PIP_DIRECTORY$PACKAGE_NAME

    echo -e "\r\nDONE!"
fi
