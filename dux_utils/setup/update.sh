#!/bin/bash
################################################################################
# Title:        Update source and pip requirements
# Description:  Check README for more information.
# Author:       Pedro Dousseau @ DUX Data
# Copyright:    DUX Data
# Date:         December 2018
# Version:      2.0.0
################################################################################

PYTHON_VERSION="null"

if [ -n "$1" ]; then
    if [ -n "$(which python$1)" ]; then
        PYTHON_VERSION="$1"
    fi
fi

if [[ $PYTHON_VERSION == "null" ]]; then
    echo "Plase, provide a installed python version as argument."
else
    echo "PYTHON VERSION: $PYTHON_VERSION"
    echo -e "\r\nUPDATING GIT REPOSITORY..."
    git pull
    echo -e "\r\nINSTALLING REQUIREMENTS..."
    eval "sudo -H python$PYTHON_VERSION -m pip install -r ../requirements.txt --force-reinstall"
    echo -e "\r\nDONE!"
fi
