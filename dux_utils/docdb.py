# -*- coding: utf-8 -*-

import tinydb


def load(path, pretty=False):
    tinydb.TinyDB.DEFAULT_TABLE = 'main'
    if pretty:
        return tinydb.TinyDB(path, sort_keys=True, indent=4, separators=(',', ': '))
    else:
        return tinydb.TinyDB(path, separators=(',', ':'))


def query():
    return tinydb.Query()


def order(result, param, desc=False, first=None, last=None):
    ordered = []
    for row in result:
        ordered.append(row)
    ordered = sorted(ordered, key=lambda k: k[param], reverse=desc)
    if first is not None:
        ordered = ordered[:first]
    if last is not None:
        ordered = ordered[-last:]
    return ordered
