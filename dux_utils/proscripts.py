# -*- coding: utf-8 -*-

from dux_utils.core import dict_merge
from argparse import ArgumentParser
from configparser import ConfigParser
from blessings import Terminal


blessings_terminal = Terminal()
blessings_colors = {
    'black': 0,
    'red': 1,
    'green': 2,
    'yellow': 3,
    'blue': 4,
    'magenta': 5,
    'cyan': 6,
    'white': 7,
    'bright_black': 8,
    'bright_red': 9,
    'bright_green': 10,
    'bright_yellow': 11,
    'bright_blue': 12,
    'bright_magenta': 13,
    'bright_cyan': 14,
    'bright_white': 15
}


def parse_arguments(arg_list, description=None, print_help=False):
    args_default = {
        'name': None,
        'abbreviation': None,
        'action': 'store',      # store (default), store_true (boolean), append (create list)
        'type': str,
        'default': None,
        'choices': None,
        'help': None,
        'required': False
    }
    parser = ArgumentParser(description=description)
    for arg in arg_list:
        arg = dict_merge(args_default, arg)
        if not arg['name'].startswith('-'):
            parser.add_argument(
                arg['name'],
                action=arg['action'],
                type=arg['type'],
                default=arg['default'],
                choices=arg['choices'],
                help=arg['help']
            )
        else:
            if arg['abbreviation'] is not None:
                if arg['action'] in ['store_true', 'store_false']:
                    parser.add_argument(
                        arg['abbreviation'],
                        arg['name'],
                        action=arg['action'],
                        help=arg['help']
                    )
                else:
                    parser.add_argument(
                        arg['abbreviation'],
                        arg['name'],
                        action=arg['action'],
                        type=arg['type'],
                        default=arg['default'],
                        choices=arg['choices'],
                        help=arg['help'],
                        required=arg['required']
                    )
            else:
                if arg['action'] in ['store_true', 'store_false']:
                    parser.add_argument(
                        arg['name'],
                        action=arg['action'],
                        help=arg['help']
                    )
                else:
                    parser.add_argument(
                        arg['name'],
                        action=arg['action'],
                        type=arg['type'],
                        default=arg['default'],
                        choices=arg['choices'],
                        help=arg['help'],
                        required=arg['required']
                    )

    args = parser.parse_args()

    if print_help:                              # Print help if no argument was passed
        if not any(vars(args).values()):
            parser.print_help()

    return args


def parse_config(conf_file):
    config = ConfigParser()
    config.read(conf_file)
    return {s: dict(config.items(s)) for s in config.sections()}


def prompt(question, default=None):
    ans = ''
    if default is None:
        while len(ans) == 0:
            ans = input("{} ".format(question))
    else:
        ans = input("{} [{}] ".format(question, default))
        if len(ans) == 0:
            ans = default
    return ans


def prompt_yn(question):
    ans = ''
    while len(ans) == 0:
        ans = input('{} [yes/no] '.format(question)).lower().strip()
    if ans in ['y', 'yes']:
        return True
    elif ans in ['n', 'no']:
        return False


def prompt_choices(question, choices):
    choices = [x.lower() for x in choices]
    ans = ''
    while ans not in choices:
        ans = input('{} [{}] '.format(question, ', '.join(choices))).lower().strip()
    return ans


def justify(cols, size=25, spacer=' ', divider=' '):
    list_sizes = type(size) is list
    ncols = len(cols)
    out = ''
    if list_sizes:
        list_sizes = len(size) == len(cols)
    for i in range(ncols):
        col_size = size
        if list_sizes:
            col_size = col_size[i]
        cols[i] = str(cols[i]).ljust(col_size, spacer)
        if i == 0:
            out = '{}{}'.format(cols[i], divider)
        elif i == ncols - 1:
            out = '{}{}'.format(out, cols[i])
        else:
            out = '{}{}{}'.format(out, cols[i], divider)
    return out


def blessings():
    return blessings_terminal


def bold(content):
    return '{}{}{}'.format(
        blessings_terminal.bold,
        content,
        blessings_terminal.normal
    )


def underline(content):
    return '{}{}{}'.format(
        blessings_terminal.underline,
        content,
        blessings_terminal.normal
    )


def color(content, fg=None, bg=None):
    if fg is None and bg is None:
        return content
    elif fg is not None and bg is None:
        return '{}{}{}'.format(
            blessings_terminal.color(blessings_colors[fg]),
            content,
            blessings_terminal.normal
        )
    elif fg is None and bg is not None:
        return '{}{}{}{}'.format(
            blessings_terminal.on_color(blessings_colors[bg]),
            blessings_terminal.color(blessings_colors['bright_white']),
            content,
            blessings_terminal.normal
        )
    else:
        return '{}{}{}{}'.format(
            blessings_terminal.color(blessings_colors[fg]),
            blessings_terminal.on_color(blessings_colors[bg]),
            content,
            blessings_terminal.normal
        )
