# DUX Utils Python Library

-   **Author:** Pedro Dousseau <mailto:[pedro@dousseau.com](mailto:pedro@dousseau.com)> @ DUX Data
-   **Date:** October 2018
-   **Copyright:** DUX Data
-   **Status:** Production

DUX Data Helper Utilities module.

## Requirements

-   Requires Python >= 3.6.
-   OS dependencies:
    - `sudo apt-get install build-essential python3-dev libmysqlclient-dev`
    - `brew install mysql-connector-c`

## Installation

Can be installed from pip or from `/dux_utils/setup/` files.

## Setup Scripts

Run setup script appropriate to the OS from its directory. Available:

- `osx.sh`
- `ubuntu.sh`
- `check.sh`
- `update.sh`

## Usage

Example:

´´´
from dux_utils import aws
´´´
