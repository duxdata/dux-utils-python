#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
#
# List of available functions for expressions:
#
#     'sin': math.sin,
#     'cos': math.cos,
#     'tan': math.tan,
#     'asin': math.asin,
#     'acos': math.acos,
#     'atan': math.atan,
#     'sqrt': math.sqrt,
#     'log': math.log,
#     'abs': abs,
#     'ceil': math.ceil,
#     'floor': math.floor,
#     'round': round,
#     'random': self.random,
#     'fac': self.fac,
#     'exp': math.exp,
#     'min': min,
#     'max': max,
#     'pyt': self.pyt,
#     'pow': math.pow,
#     'atan2': math.atan2,
#     'E': math.e,
#     'PI': math.pi

from dux_utils.exparser import Parser


def main():
    exparser = Parser()
    equation = exparser.parse('x + 3 * 5')
    result = equation.evaluate({'x': 2})
    print(result)


if __name__ == '__main__':
    main()
