import json
from dux_utils import shell


print(
    json.dumps(shell.sysinfo(system=True, cpu=True, memory=True, disk=True, network=False, conn_filter=['LISTEN', 'ESTABLISHED']), indent=4)
)
