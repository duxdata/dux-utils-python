import json
from dux_utils.shell import pinfo

# REQUIRES ROOT

# Get mysql by port
print(
    json.dumps(pinfo(pports=[3306]), indent=4)
)

# Get mysql by name
print(
    json.dumps(pinfo(pnames=['mysql']), indent=4)
)

# Get processes running on python
print(
    json.dumps(pinfo(pcmds=['python']), indent=4)
)
