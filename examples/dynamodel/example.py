from dux_utils.dynamodel import Dynamodel
from dux_utils.dynamodel import dynattribute


class MyModel(Dynamodel):

    class Meta:
        table_name = 'my-dynamo-table-name'
        region = 'us-west-2'

    schema = {
        'forum_name': str,
        'subjects': list,
        'views': float,
        'notes': list,
        'options': [
            {
                'option_name': str,
                'enable': bool
            }
        ]
    }

    forum_name = dynattribute('string')(hash_key=True)
    subjects = dynattribute('list')(default=[])
    views = dynattribute('number')(default=0)
    notes = dynattribute('list')(default=[])
    options = dynattribute('json')(null=True)


def create_item():
    # arguments can be hash_key (required), range_key (optional), other_attributes(optional)
    item = MyModel('hash_key_value', views=10)
    item.save()


def update_item():
    item = MyModel.get('hash_key_value')
    item.update([
        MyModel.views.set(10),
        MyModel.notes.append(['test'])
    ])
    # gets other updates from the database
    item.refresh()


def delete_item():
    item = MyModel.get('hash_key_value')
    item.delete()


def main():
    create_item()
    update_item()


if __name__ == '__main__':
    main()
