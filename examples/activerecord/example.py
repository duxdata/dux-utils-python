#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

from dux_utils.activerecord import Database, Schema, ActiveRecord


class User(ActiveRecord):

    # __guarded__ = ['email']
    pass


def main():

    config = {
        'mysql': {
            'driver': 'mysql',
            'host': 'localhost',
            'database': 'tempdb',
            'user': 'root',
            'password': 'cooper10'
        }
    }

    database = Database(config)

    schema = Schema(config)

    if schema.has_table('users'):
        print('Users table already exists.')
    else:
        with schema.create('users') as table:
            table.string('email')
            table.timestamps()

    User.setdb(database)

    User.create({
        'email': 'pedro@sdas.dcom'
    })

    for user in User.all():
        print(user.email)


if __name__ == '__main__':
    main()
