from dux_utils.io import socket


async def callback(reader, writer):
    while True:
        data = await reader.read(255)
        message = data.decode()
        print('GOT MESSAGE > {}'.format(message))
        if not message:
            break
        elif 'quit' in message:
            writer.write('BYE!'.encode())
            await writer.drain()
            writer.close()
            break
        else:
            writer.write('{}'.format(message).encode())
            await writer.drain()
    print('<END>')

socket('hisock', callback, mode='server', type='unix')
