from dux_utils.io import socket


def callback(sock):

    while True:
        message = ''
        sock.sendall(input('Client > ').encode())
        while len(message) == 0:
            message = sock.recv(1024).decode()
        print('Server > {}'.format(message))

    print('<END>')


socket('hisock', callback, mode='client', type='unix')
