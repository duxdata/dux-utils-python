from dux_utils.io import http
from bottle import request


def hello():
    if request.params.name:
        return 'Hello {}!'.format(request.params.name)
    return "Hello Weird!"


def testjson():
    return {
        'title': 'Such a nice dict'
    }


routes = [
    {
        'method': 'GET',
        'path': '/hello',
        'callback': hello
    },
    {
        'method': 'GET',
        'path': '/test-json',
        'callback': testjson
    }
]

http('127.0.0.1', 8080, routes, reloader=True, quiet=True)
