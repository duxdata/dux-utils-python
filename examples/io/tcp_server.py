from dux_utils.io import socket


async def callback(reader, writer):
    while True:
        data = await reader.read(255)
        message = data.decode()
        addr = writer.get_extra_info('peername')
        print('MESSAGE FROM {} > {}'.format(addr, message))
        if not message:
            break
        elif 'quit' in message:
            writer.write('BYE!'.encode())
            await writer.drain()
            writer.close()
            break
        else:
            writer.write('{}'.format(message).encode())
            await writer.drain()
    print('<END>')

socket('localhost:5555', callback, mode='server', type='tcp')
