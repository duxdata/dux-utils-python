#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
#
#   Resample frequencies:
#           B         business day frequency
#           C         custom business day frequency (experimental)
#           D         calendar day frequency
#           W         weekly frequency
#           M         month end frequency
#           SM        semi-month end frequency (15th and end of month)
#           BM        business month end frequency
#           CBM       custom business month end frequency
#           MS        month start frequency
#           SMS       semi-month start frequency (1st and 15th)
#           BMS       business month start frequency
#           CBMS      custom business month start frequency
#           Q         quarter end frequency
#           BQ        business quarter endfrequency
#           QS        quarter start frequency
#           BQS       business quarter start frequency
#           A         year end frequency
#           BA, BY    business year end frequency
#           AS, YS    year start frequency
#           BAS, BYS  business year start frequency
#           BH        business hour frequency
#           H         hourly frequency
#           T, min    minutely frequency
#           S         secondly frequency
#           L, ms     milliseconds
#           U, us     microseconds
#           N         nanoseconds


from dux_utils import timeseries


def apply():
    data = timeseries.csv_file('data/29.csv', cols=[1, 8, 11])

    def dosomething(row):
        row[2] = row[0] + row[1]
        return row

    return data.apply(dosomething, axis='columns')


def resample():
    data = timeseries.csv_file('data/29.csv', cols=[1, 8, 11])
    return data.resample('H').mean()


def concat():
    columns = [1, 8, 11]
    data = []
    data.append(timeseries.csv_file('data/27.csv', cols=columns))
    data.append(timeseries.csv_file('data/28.csv', cols=columns))
    data.append(timeseries.csv_file('data/29.csv', cols=columns))
    data.append(timeseries.csv_file('data/30.csv', cols=columns))
    data = timeseries.pandas().concat(data)
    return data.resample('H').mean()


def group():
    daily_data = []
    data = concat()
    days = data.index.to_period('D')
    for group in data.groupby(days):
        daily_data.append({
            'day': group[0],
            'data': timeseries.pandas().DataFrame(group[1]).sort_index()
        })
        print('# Date: {}'.format(daily_data[-1]['day']))
        print(daily_data[-1]['data'])
        print()


def main():

    print(apply())


if __name__ == '__main__':
    main()
